package com.tripadvisor.plagiarism.service.util;

import com.tripadvisor.plagiarism.util.FileReader;

import java.util.Map;

/**
 * Class is responsible for storing synonyms and is used for substituting synonym words to a single one
 */
public final class SynonymController {

    private final Map<String, String> synonyms;

    public SynonymController(String fileName) {
        this.synonyms = FileReader.readFileAndReturnMap(fileName);
    }

    /**
     * Replaces specific words to a synonym is exists
     *
     * @param word from text
     * @return word synonym if exists otherwise argument
     */
    public String getSynonymForWord(String word) {
        if (synonyms.containsKey(word)) {
            return synonyms.get(word);
        }
        return word;
    }

}
