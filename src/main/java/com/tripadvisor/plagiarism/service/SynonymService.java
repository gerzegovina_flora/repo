package com.tripadvisor.plagiarism.service;

/**
 * If a word has synonym it means that the synonym can be replaced to unified value.
 * It wont deteriorate accuracy and in addition no need to check each time synonym for word.
 * So the interface replaces synonym to only word to be sure that we check just words that can
 * impact on comparison.
 */
public interface SynonymService {

    /**
     * Checks input word for a synonyms. If synonym exists, removes from text
     *
     * @param word is input word from file
     * @return original word if synonyms do not exist otherwise remove word
     */
    String replaceSynonymIfExists(String word);
}
