package com.tripadvisor.plagiarism;

/**
 * The interface embarks execution of seeking matches in texts.
 * Has the only implementation {@link com.tripadvisor.plagiarism.impl.PlagiarismServiceImpl}
 */
public interface PlagiarismService {
    /**
     * Method performs two files comparison. It takes into account word synonyms.
     *
     * @return percentage of matches
     */
    double perform();
}
