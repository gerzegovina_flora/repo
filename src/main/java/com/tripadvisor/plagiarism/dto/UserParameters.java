package com.tripadvisor.plagiarism.dto;

public class UserParameters {

    private int tupleSize;
    private String synonymsFile;
    private String file1;
    private String file2;

    public int getTupleSize() {
        return tupleSize;
    }

    public void setTupleSize(int tupleSize) {
        this.tupleSize = tupleSize;
    }

    public String getSynonymsFile() {
        return synonymsFile;
    }

    public void setSynonymsFile(String synonymsFile) {
        this.synonymsFile = synonymsFile;
    }

    public String getFile1() {
        return file1;
    }

    public void setFile1(String file1) {
        this.file1 = file1;
    }

    public String getFile2() {
        return file2;
    }

    public void setFile2(String file2) {
        this.file2 = file2;
    }

}
