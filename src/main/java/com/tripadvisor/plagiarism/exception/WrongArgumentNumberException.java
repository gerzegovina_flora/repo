package com.tripadvisor.plagiarism.exception;

/**
 * Throws when user puts invalid number of arguments. See {@link com.tripadvisor.plagiarism.PlagiarismDetection}
 */
public class WrongArgumentNumberException extends PlagiarismDetectorException {

    public WrongArgumentNumberException(String message) {
        super(message);
    }
}
