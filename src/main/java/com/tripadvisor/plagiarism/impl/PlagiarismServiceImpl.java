package com.tripadvisor.plagiarism.impl;

import com.tripadvisor.plagiarism.PlagiarismService;
import com.tripadvisor.plagiarism.dto.UserParameters;
import com.tripadvisor.plagiarism.service.SynonymService;
import com.tripadvisor.plagiarism.service.WordPreProcessor;
import com.tripadvisor.plagiarism.service.impl.DefaultSynonymService;
import com.tripadvisor.plagiarism.service.impl.DefaultWordPreProcessor;
import com.tripadvisor.plagiarism.util.FileReader;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PlagiarismServiceImpl implements PlagiarismService {

    private static final String SPACE_STRING = " ";

    private final UserParameters userParameters;
    private final SynonymService synonymService;
    private final WordPreProcessor wordPreProcessor;

    public PlagiarismServiceImpl(UserParameters userParameters) {
        this.userParameters = userParameters;
        this.synonymService = new DefaultSynonymService(userParameters.getSynonymsFile());
        this.wordPreProcessor = new DefaultWordPreProcessor();
    }

    @Override
    public double perform() {
        String textFromFile1 = FileReader.readFileAndReturnText(userParameters.getFile1());
        String textFromFile2 = FileReader.readFileAndReturnText(userParameters.getFile2());

        Set<String> wordsFromFile1 = getWords(textFromFile1, userParameters.getTupleSize());
        if (!wordsFromFile1.isEmpty()) {
            Set<String> wordsFromFile2 = getWords(textFromFile2, userParameters.getTupleSize());
            return computeMatchPercentage(wordsFromFile1, wordsFromFile2);
        }
        return 0;
    }

    private double computeMatchPercentage(Set<String> wordsFromFile1, Set<String> wordsFromFile2) {
        int count = getNumberOfMatches(wordsFromFile1, wordsFromFile2);
        return getPercentage(wordsFromFile1.size(), count);
    }

    private int getNumberOfMatches(Set<String> wordsFromFile1, Set<String> wordsFromFile2) {
        int count = 0;
        for (String word : wordsFromFile1) {
            if (wordsFromFile2.contains(word)) {
                count++;
            }
        }
        return count;
    }

    private float getPercentage(int wordsNumber, float count) {
        return (count * 100) / wordsNumber;
    }

    private Set<String> getWords(String text, int tupleSize) {
        String[] words = text.split(SPACE_STRING);

        Set<String> concatenatedWords = new HashSet<>();

        IntStream.range(0, words.length - tupleSize + 1).forEach(index -> {
            String handledText = Arrays.stream(Arrays.copyOfRange(words, index, index + tupleSize))
                    .map(wordPreProcessor::handle)
                    .map(synonymService::replaceSynonymIfExists)
                    .collect(Collectors.joining());
            concatenatedWords.add(handledText);
        });

        return concatenatedWords;
    }
}
