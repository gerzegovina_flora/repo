package com.tripadvisor.plagiarism;

import com.tripadvisor.plagiarism.dto.UserParameters;
import com.tripadvisor.plagiarism.exception.WrongArgumentNumberException;
import com.tripadvisor.plagiarism.impl.PlagiarismServiceImpl;

public class PlagiarismDetection {

    private static final int DEFAULT_NUMBER_OF_PARAMETERS = 3;

    private final PlagiarismService plagiarismService;

    private PlagiarismDetection(UserParameters userParameters) {
        this.plagiarismService = new PlagiarismServiceImpl(userParameters);
    }

    public static void main(String[] args) {
        try {
            PlagiarismDetection plagiarismDetection = new PlagiarismDetection(wrapUserParameters(args));
            plagiarismDetection.performPlagiarism();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void performPlagiarism() {
        System.out.println(plagiarismService.perform() + "%");
    }

    private static UserParameters wrapUserParameters(String[] parameters) {
        if (parameters.length >= DEFAULT_NUMBER_OF_PARAMETERS) {
            UserParameters userParameters = new UserParameters();
            userParameters.setSynonymsFile(parameters[0]);
            userParameters.setFile1(parameters[1]);
            userParameters.setFile2(parameters[2]);
            userParameters.setTupleSize(getTupleSize(parameters));
            return userParameters;
        }
        throw new WrongArgumentNumberException("Number of arguments is invalid");
    }

    private static Integer getTupleSize(String[] parameters) {
        if (parameters.length > DEFAULT_NUMBER_OF_PARAMETERS) {
            return Integer.valueOf(parameters[3]);
        }
        return DEFAULT_NUMBER_OF_PARAMETERS;
    }

}