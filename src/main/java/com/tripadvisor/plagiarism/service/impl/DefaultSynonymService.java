package com.tripadvisor.plagiarism.service.impl;

import com.tripadvisor.plagiarism.service.SynonymService;
import com.tripadvisor.plagiarism.service.util.SynonymController;

public class DefaultSynonymService implements SynonymService {

    private final SynonymController synonymController;

    public DefaultSynonymService(String fileName) {
        synonymController = new SynonymController(fileName);
    }

    @Override
    public String replaceSynonymIfExists(String word) {
        return synonymController.getSynonymForWord(word);
    }

}
