package com.tripadvisor.plagiarism.util;

import com.tripadvisor.plagiarism.exception.InvalidFileStateException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Util class which is in charge of reading content from files.
 * The class takes text from file and converts to either single line or map
 */
public final class FileReader {

    private static final int FIRST_ELEMENT = 0;

    private FileReader() {
        throw new IllegalStateException("FileReader can not be instantiated");
    }

    /**
     * Used for reading file which is defined by user input
     *
     * @param fileName is a user's input file name
     * @return list all words in file
     */
    public static String readFileAndReturnText(String fileName) {
        try {
            if (isFileValid(fileName)) {
                return Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8)
                        .stream()
                        .reduce((line1, line2) -> line1 + line2)
                        .orElseThrow(IllegalArgumentException::new);
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return "";
    }

    /**
     * Used for reading file with synonyms which is defined by user input
     *
     * @param fileName is a user's input file name for synonyms
     * @return map with words and list of associated words
     */
    public static Map<String, String> readFileAndReturnMap(String fileName) {
        try {
            if (isFileValid(fileName)) {
                Map<String, String> map = new HashMap<>();

                List<String> lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
                for (String line : lines) {
                    List<String> words = Arrays.asList(line.split(" "));
                    for (String word : words) {
                        map.put(word, words.get(FIRST_ELEMENT));
                    }
                }
                return map;
            }
        } catch (IOException e) {
            throw new RuntimeException("Something went wrong with the file: " + fileName);
        }
        return Collections.emptyMap();
    }

    private static boolean isFileValid(String fileName) throws IOException {
        if (Files.exists(Paths.get(fileName)) && Files.size(Paths.get(fileName)) > 0) {
            return true;
        }
        throw new InvalidFileStateException("File " + fileName + " is empty or does not exist");
    }

}
