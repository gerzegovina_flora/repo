package com.tripadvisor.plagiarism.service.impl;

import com.tripadvisor.plagiarism.service.WordPreProcessor;

public class DefaultWordPreProcessor implements WordPreProcessor {

    private static final String ALL_EXTRA_SYMBOLS = "[-+.^:,]";

    @Override
    public String handle(String word) {
        return word.replaceAll(ALL_EXTRA_SYMBOLS, "").toLowerCase();
    }
}
