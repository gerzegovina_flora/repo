package com.tripadvisor.plagiarism.service;

import com.tripadvisor.plagiarism.service.impl.DefaultWordPreProcessor;

/**
 * Words can have extra symbols after splitting. In fact, the same words can be identified as different in case when
 * extra characters take place. The {@link DefaultWordPreProcessor} removes them.
 */
public interface WordPreProcessor {

    /**
     * Removes specific characters from input such as + . , = and so on
     *
     * @param word is input word from file
     * @return word without specific symbols
     */
    String handle(String word);

}
