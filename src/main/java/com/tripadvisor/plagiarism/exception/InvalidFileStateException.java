package com.tripadvisor.plagiarism.exception;

/**
 * Throws when file is empty or does not exist. See {@link com.tripadvisor.plagiarism.util.FileReader}
 */
public class InvalidFileStateException extends PlagiarismDetectorException {

    public InvalidFileStateException(String message) {
        super(message);
    }
}
