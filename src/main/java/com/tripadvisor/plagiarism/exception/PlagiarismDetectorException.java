package com.tripadvisor.plagiarism.exception;

/**
 * Parent exception for {@link com.tripadvisor.plagiarism.exception.InvalidFileStateException} and {@link com.tripadvisor.plagiarism.exception.WrongArgumentNumberException}
 */
class PlagiarismDetectorException extends RuntimeException {

    PlagiarismDetectorException(String message) {
        super(message);
    }
}
